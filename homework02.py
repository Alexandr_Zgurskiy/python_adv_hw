from pprint import pprint


def slice3(board):
    board_tr = list()
    s = (board[0:3], board[3:6], board[6:9], board[0:7:3], board[1:8:3], board[2:9:3], board[::4], board[3:8:2])
    for x in s:
        board_tr.append(x)

    return board_tr


def outcome(board):
    c = 0
    for i in board:
        for j in i:
            if j == None:
                c = 1
    if (0, 0, 0) in board or (1, 1, 1) in board:
       game_outcome = 1
    elif ((0, 0, 0)not in board or (1, 1, 1)not in board) and c != 1:
        game_outcome = 2
    else:
        game_outcome = 3
    return game_outcome

X, O, _ = 1, 0, None

TEST_BOARD = (
    X, X, _,
    O, O, X,
    X, O, O
)

TEST_BOARD = slice3(TEST_BOARD)
pprint(TEST_BOARD)
print(outcome(TEST_BOARD))